// -*- mode: C++ -*-
//
//  A class for grabbing data from scanned graphs
//  Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
#ifndef DATASTEALER_CALC_H
#define DATASTEALER_CALC_H
#include <TMath.h>
#include "Axis.h"
#include "Point.h"

namespace DataStealer
{
  struct Calculator
  {
    /** Constructor */
    Calculator();

    void Cache(const Axis& xLeft,   const Axis& xRight,
	       const Axis& yBottom, const Axis& yTop,
	       bool        xlog,    bool        ylog,
	       bool        rnd,     int         rndBias);
    /** Calculate real coordinates relative to @f$(xx,xy)@f$ and 
	@f$(yx,yy)@f$. 
	@param xx X coordinate of X reference point
	@param xy Y coordinate of X reference point
	@param yx X coordinate of Y reference point
	@param yy Y coordinate of Y reference point 
	@param x  On return, the graph X coordinate
	@param y  On return, the graph Y coordinate */
    void Pixel2Coord(Double_t  xx,      Double_t  xy, 
		     Double_t  yx,      Double_t  yy,
		     Double_t& x,       Double_t& y) const; 
    /** Calculate real coordinates relative to @f$(xx,xy)@f$ and 
	@f$(yx,yy)@f$. 
	@param ix X pixel
	@param iy Y pixel 
	@param x  On return, the graph X coordinate
	@param y  On return, the graph Y coordinate */
    void Pixel2Coord(Int_t     ix,      Int_t iy, 
		     Double_t& x,       Double_t& y) const; 
    /** Calculate the real coordinate value @f$ (x,y)@f$ corresponding
	to pixel value @f$ (px,py)@f$ 
	@param px Pixel X coordinate
	@param py Pixel Y coordinate
	@param x  On return, the graph X coordinate
	@param y  On return, the graph Y coordinate */
    void Pixel2Value(Int_t px, Int_t py, Double_t& x, Double_t& y) const;
    /** Calculate the real coordinate value @f$ (x,y)@f$ corresponding
	to pixel value @f$ (px,py)@f$.  This also calculates the
	precision for each point  
	@param px Pixel X coordinate
	@param py Pixel Y coordinate
	@param x  On return, the graph X coordinate
	@param y  On return, the graph Y coordinate 
	@param ex On return, the graph X coordinate precision
	@param ey On return, the graph Y coordinate precision  */
    void Pixel2Value(Int_t     px, Int_t     py,
		     Double_t& x,  Double_t& y,
		     Double_t& ex, Double_t& ey) const;
    /** Calculate the real coordinate value @f$ (x,y)@f$ corresponding
	to pixel value @f$ (px,py)@f$.  This also calculates the
	precision for each point  
	@param px Pixel X coordinate
	@param py Pixel Y coordinate
	@param p  Point to set 
    */
    void Pixel2Value(Int_t px, Int_t py, Point* p) const;
    /** Calculate the real uncertainty values @f$ (ex,ey)@f$ corresponding
	to pixel value @f$ (px,py)@f$ 
	@param px Pixel X coordinate
	@param py Pixel Y coordinate
	@param vy X value 
	@param vy Y value 
	@param x  On return, the graph X coordinate
	@param y  On return, the graph Y coordinate */
    void Pixel2Error(Int_t px,     Int_t py,
		     Double_t  vx, Double_t vy, 
		     Double_t& x,  Double_t& y) const;
    /** Round value by precision (w/optional bias) 
	@param v  Value 
	@param e  Precision 
	@param o  Bias 
    */
    static Double_t Round(Double_t v, Double_t e, Int_t bias=0);

    /** Definition of an axis point */
    struct Def
    {
      /** X pixel of point */
      Int_t    fXpixel;
      /** Y pixel of point */
      Int_t    fYpixel;
      /** Value at point */
      Double_t fValue;
      /** (log) value at point */
      Double_t fLogValue;
      /** Set from external */
      void Set(const Axis& a, bool isLog)
      {
	fXpixel   = a.fXpixel;
	fYpixel   = a.fYpixel;
	fValue    = a.GetValue();
	fLogValue = isLog ? log(fValue) : fValue;
      }
    };
    /** Axis definition */
    struct Ax
    {
      /** Low (left, bottom) definition point */
      Def    fLow;
      /** High (right, top) definition point */
      Def    fHigh;
      /** Change along X in pixels */
      double fDeltaX;
      /** Change along Y in pixels */
      double fDeltaY;
      /** Is this logarithmic */
      bool   fIsLog;
      /** Set from external */
      void Set(const Axis& low, const Axis& high, bool isLog)
      {
	fDeltaX = double(high.fXpixel - low.fXpixel);
	fDeltaY = double(high.fYpixel - low.fYpixel);
	fIsLog  = isLog;

	fLow .Set(low, isLog);
	fHigh.Set(high, isLog);
      }
    };
    /** X axis definition */
    Ax fX;
    /** Y axis definition */
    Ax fY;
    
    /** Whether we're rounding by precision */
    Bool_t                fRound;
    /** Rounding bias */
    Int_t                 fRoundBias;
    
  };

  //==================================================================
  inline
  Calculator::Calculator()
    : fRound(0),	// Whether we're rounding by precision
      fRoundBias(0)	// Rounding bias
  {}
  //------------------------------------------------------------------
  inline void
  Calculator::Cache(const Axis& xLeft,   const Axis& xRight,
		    const Axis& yBottom, const Axis& yTop,
		    bool        xLog,    bool        yLog,
		    bool        rnd,     int         rndBias)
  {
    fX.Set(xLeft,   xRight, xLog);
    fY.Set(yBottom, yTop,   yLog);

    // Get options for rounding
    fRound        = rnd;
    fRoundBias    = rndBias;
  }
  //____________________________________________________________________
  inline void 
  Calculator::Pixel2Coord(Double_t  xx,      Double_t  xy, 
			  Double_t  yx,      Double_t  yy,
			  Double_t& x,       Double_t& y) const
  {    

    double alpha = (xx - xy * (fY.fDeltaX / fY.fDeltaY)) / 
      (fX.fDeltaX - fX.fDeltaY * fY.fDeltaX / fY.fDeltaY);
    double beta  = (yy - yx * (fX.fDeltaY / fX.fDeltaX)) / 
      (fY.fDeltaY - fY.fDeltaX * fX.fDeltaY / fX.fDeltaX);
  
    x = -alpha * (fX.fHigh.fLogValue - fX.fLow.fLogValue) + fX.fLow.fLogValue;
    if (fX.fIsLog) x = TMath::Exp(x);

    y = -beta * (fY.fHigh.fLogValue - fY.fLow.fLogValue) + fY.fLow.fLogValue;
    if (fY.fIsLog) y = TMath::Exp(y);
  }
  //____________________________________________________________________
  inline void 
  Calculator::Pixel2Coord(Int_t     ix, Int_t     iy,
			  Double_t& x,  Double_t& y) const
  {
    Double_t xp = Double_t(ix);
    Double_t yp = Double_t(iy);
      
    Pixel2Coord(fX.fLow.fXpixel - xp, fX.fLow.fYpixel - yp, 
		fY.fLow.fXpixel - xp, fY.fLow.fYpixel - yp,
		x,                    y);
  }
    
  //____________________________________________________________________
  inline void 
  Calculator::Pixel2Value(Int_t ix, Int_t iy, Double_t& x, Double_t& y) const
  {
    // Calculate the coordinates of a pixel picked
    if (!fRound) {
      Pixel2Coord(ix, iy, x, y);
      return;
    }
    Double_t ex, ey;
    Pixel2Value(ix, iy, x, y, ex, ey);
    x = Round(x, ex, fRoundBias);
    y = Round(y, ey, fRoundBias);
  }

  //____________________________________________________________________
  inline void 
  Calculator::Pixel2Value(Int_t     ix, Int_t     iy,
			  Double_t& x,  Double_t& y,
			  Double_t& ex, Double_t& ey) const
  {
    // Calculate the coordinates of a pixel picked
    Pixel2Coord(ix, iy, x, y);

    // Calculate the coordinates of the pixel over and right to the picked
    Pixel2Coord(ix+1, iy+1, ex, ey);

    // Calculate the coordinates of the pixel below and left to the picked
    double xe, ye;
    Pixel2Coord(ix-1, iy-1, xe, ye);

    // Calculate the distance between pixels
    ex -= xe;
    ey -= ye;
  
    // Calculate the error 
    ex = TMath::Abs(ex / 4.0);
    ey = TMath::Abs(ey / 4.0);
  }
  //____________________________________________________________________
  inline void 
  Calculator::Pixel2Value(Int_t ix, Int_t iy, Point* p) const
  {
    Double_t vx, vy;
    Pixel2Value(ix,iy,vx,vy);
    p->SetPixelsValues(ix,iy,vx,vy);
  }
  //____________________________________________________________________
  inline void 
  Calculator::Pixel2Error(Int_t     ix, Int_t     iy,
			  Double_t  vx, Double_t  vy,
			  Double_t& ex, Double_t& ey) const
  {
    Pixel2Value(ix, iy, ex, ey);
    ex = TMath::Abs(vx - ex);
    ey = TMath::Abs(vy - ey);
  }
    
  
  //__________________________________________________________________
  inline Double_t 
  Calculator::Round(Double_t v, Double_t e, Int_t o)
  {
    // Find the exponent we need to round to 
    Int_t    n = TMath::Ceil(TMath::Log10(e))-o;
    Double_t m = TMath::Power(10,-n);
    return TMath::Floor(v * m + .5) / m;
  }
  
  
}
#endif

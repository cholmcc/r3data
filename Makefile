#
#
#
prefix		= /usr
PACKAGE		= datastealer
VERSION		= 0.1
DISTDIR		= $(PACKAGE)-$(VERSION)
SOURCES		= $(PACKAGE).C TASImage.h
HEADERS		= Axis.h Current.h Image.h Main.h Point.h Store.h LinkDef.h
TESTS		= $(filter-out %/CVS, $(wildcard test/*))
DOCS		= README screenshot1.png screenshot2.png
DISTS		= $(SOURCES) $(HEADERS) $(TESTS) $(DOCS) Makefile
ROOT_CFLAGS	= $(shell root-config --cflags) 
ROOT_LIBS	= $(shell root-config --glibs) -lGed
ROOT_LIBDIR	= $(shell root-config --libdir)
ROOT_SOVERS	= $(dir $(shell root-config --version))
ROOT_PLUGDIR    = $(ROOT_LIBDIR)/root$(dir $(ROOT_SOVERS))

CPP		= g++ -E
CPPFLAGS	= $(ROOT_CFLAGS)
CINTFLAGS	= $(filter -I%, $(ROOT_CFLAGS))
CXX		= g++ -c
CXXFLAGS	= -g -Wall -fPIC
LD		= g++ 
LDFLAGS		= $(ROOT_LIBS) -L$(ROOT_PLUGDIR) \
		 -Wl,-rpath,$(ROOT_PLUGDIR) -lASImage

%.o:%.C
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $< 

$(PACKAGE):Dict.o  $(PACKAGE).o 
	$(LD)  -o $@ $^ $(LDFLAGS)

Dict.C: $(HEADERS)
	rootcint -f $@ -c $(CINTFLAGS) $^

all:	$(PACKAGE)

install: all
	mkdir -p $(DESTDIR)$(prefix)/bin
	cp $(PACKAGE) $(DESTDIR)$(prefix)/bin/$(PACKAGE)

uninstall:
	rm -f $(DESTDIR)$(prefix)/bin/$(PACKAGE)

clean:
	rm -rf $(PACKAGE) *.o Dict.* file* *~ $(PACKAGE)_C.*
	rm -rf *.so *.pcm *.d 
	-(cd debian && rm -rf datastealer *.subtvars *.debhelper)

dist:	$(DISTS)
	mkdir -p $(DISTDIR)
	mkdir -p $(DISTDIR)/test
	$(foreach f, $(DISTS), cp $(f) $(DISTDIR)/$f;)
	tar -czvf $(DISTDIR).tar.gz $(DISTDIR)
	rm -rf  $(DISTDIR)

deb:    debian
debian: dist
	mv $(PACKAGE)_$(VERSION).orig.tar.gz ../
	dpkg-buildpackage -rfakeroot -i.git


debian:	DISTDIR=$(PACKAGE)_$(VERSION).orig

.PHONY: debian deb

#
# EOF
#




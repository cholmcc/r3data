// -*- mode: C++ -*-
//
//  A class for grabbing data from scanned graphs
//  Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
// This code was heavily inspired by G3Data available from 
// http://www.frantz.fi/software/g3data.php.  The copyright of G3Data
// follows because some parts of this code is more or less lifted
// directly from there:
//
//  Copyright (C) 2000 Jonas Frantz <jonas.frantz@welho.com>
//
//  This file is part of g3data.
//
//  g3data is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  g3data is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef DATASTEALER_IMAGE
#define DATASTEALER_IMAGE
#include "TASImage.h"
#define BASE TASImage
#include <TPad.h>
#include <iostream>

namespace DataStealer 
{
  //====================================================================
  /** Specialisation of TImage to facilitate mapping pad coordinates
      to image coordinates.  */
  struct Image : public BASE
  {
    /** Constructor */
    Image(const char* filename) : BASE(filename) {}
    /** Connstructor */
    Image(Int_t w, Int_t h) : BASE(w, h) {}
    /** Copy an area of the image into @a dst
	@param dst   Destination image
	@param xsrc  X coordinate to start from in this image
	@param ysrc  Y coordinate to start from in this image
	@param w     Width of area to copy
	@param h     Height of area to copy
	@param xdst  X coordinate in @a dst to copy to 
	@param ydst  Y coordinate in @a dst to copy to 
	@param gfunc Graphics function.
	@param chan  Colour channel */
    void CopyArea(TImage* dst, Int_t xsrc, Int_t ysrc, UInt_t w, UInt_t h, 
		  Int_t xdst = 0, Int_t ydst = 0, Int_t gfunc = 3, 
		  TImage::EColorChan chan = TImage::kAllChan);
    /** Convert pad coordinates to image coordinates
	@param px Pad X coordinate
	@param py Pad Y coordinate
	@param ix On return, the image X coordinate
	@param iy On return, the image Y coordinate */
    void PadToImage(Int_t px, Int_t py, Int_t& ix, Int_t& iy);
    /** Execute an event */
    void ExecuteEvent(Int_t event, Int_t px, Int_t py);
    /** Destructor */
    virtual ~Image() {}
    ClassDef(Image,0);
  };
  //__________________________________________________________________
  inline void 
  Image::CopyArea(TImage* dst, 
		  Int_t   xsrc, 
		  Int_t   ysrc, 
		  UInt_t  w, 
		  UInt_t  h, 
		  Int_t   xdst, 
		  Int_t   ydst, 
		  Int_t   gfunc, 
		  TImage::EColorChan chan) 
  {
    GetArgbArray();
    TImage* adst = ((TImage*)dst);
    adst->GetArgbArray();
    Int_t xstart  = (xsrc >= 0 ? xsrc : 0);
    Int_t ystart  = (ysrc >= 0 ? ysrc : 0);
    Int_t xwidth  = (xsrc >= 0 ? w    : w+xsrc);
    Int_t ywidth  = (ysrc >= 0 ? h    : h+ysrc);
    Int_t xtarget = (xsrc >= 0 ? xdst : xdst-xsrc);
    Int_t ytarget = (ysrc >= 0 ? ydst : ydst-ysrc);
    BASE::CopyArea(dst, xstart, ystart, xwidth, ywidth, 
		       xtarget, ytarget, gfunc, chan);
  }
  //__________________________________________________________________
  inline void 
  Image::PadToImage(Int_t px, Int_t py, Int_t& ix, Int_t& iy) 
  {
    TImage*  s     = GetScaledImage();
    Int_t    w     = (s ? s->GetWidth()  : GetWidth());
    Int_t    h     = (s ? s->GetHeight() : GetHeight());
    Double_t xfact = (s ? Double_t(w) / fZoomWidth  : 1);
    Double_t yfact = (s ? Double_t(h) / fZoomHeight : 1);
    //std::cout << "xfact="<<xfact << ",yfact="<<yfact << std::endl;
    ix             = px - gPad->XtoAbsPixel(0);
    //iy           = py - gPad->YtoAbsPixel(1);
    iy             = gPad->YtoAbsPixel(1) - py;
    iy             = h - iy;
    ix             = Int_t(ix / xfact) + fZoomOffX;
    iy             = Int_t(iy / yfact) + fZoomOffY;
    //std::cout <<"xoff="<<fZoomOffX <<"yoff="<<fZoomOffY << std::endl;
  }
  //__________________________________________________________________
  inline void 
  Image::ExecuteEvent(Int_t event, Int_t px, Int_t py)
  {
    // Execute mouse events.
    gPad->SetCursor(kCross);
    if (!IsEditable()) return;
    gPad->ExecuteEvent(event, px, py);
  }
}
#endif 
//__________________________________________________________________
//
// EOF
//



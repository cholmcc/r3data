// -*- mode: C++ -*-
//
//  A class for grabbing data from scanned graphs
//  Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
// This code was heavily inspired by G3Data available from 
// http://www.frantz.fi/software/g3data.php.  The copyright of G3Data
// follows because some parts of this code is more or less lifted
// directly from there:
//
//  Copyright (C) 2000 Jonas Frantz <jonas.frantz@welho.com>
//
//  This file is part of g3data.
//
//  g3data is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  g3data is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef DATASTEALER_AXIS
#define DATASTEALER_AXIS
#include <TMarker.h>
#include <RQ_OBJECT.h>
#include <TFrame.h>
#include <TGNumberEntry.h>
#include <TGButton.h>
#include <TPad.h>

namespace DataStealer 
{
  struct Main;

  //====================================================================
  /** @class Axis 
      @brief A reference point on an axis, and the GUI element to
      input the values. */
  struct Axis : public TMarker, public TQObject
  {
    /** Constructor 
	@param m Pointer to main frame 
	@param name Name of this 
	@param color Color of this */
    Axis(TGCompositeFrame& left, const char* name, Int_t color);
    /** Destructor */
    virtual ~Axis() { }
    /** Get the name */
    const char* GetName() const { return fName.Data(); }
    /** Clear this */ 
    void Clear(Option_t* option=""); //*MENU*
    /** Handle when button is pressed. */
    void HandleButton() { Define(this); }
    /** Handle value change */
    void HandleChanged(Long_t) { Changed(); }
    /** Signal to emit */
    void Define(Axis*); // *SIGNAL*
    /** Signal to emit */
    void Changed() { Emit("Changed()"); } //*SIGNAL*
    /** Set the @f$(x,y)@f$ reference point. */
    void SetPixels(Int_t x, Int_t y);
    /** Check if this reference point has been set. */
    Bool_t   IsSet() const { return fIsSet; }
    /** Delete this point. */
    void     Delete(Option_t* option="") { Clear(option); } //*MENU*
    /** Gethe value */
    Double_t GetValue() const { return fEntry.GetNumber(); }
    /** Take focus. */
    void     Focus();

    /** Pixel X coordinate */
    Int_t fXpixel;
    /** Pixel Y coordinate */
    Int_t fYpixel;
  protected: 
    /** Name */
    TString            fName;
    /** Layout hints */
    TGLayoutHints      fFrameHints;
    /** Frame */
    TGHorizontalFrame  fFrame;
    /** Layout hints */
    TGLayoutHints      fButtonHints;
    /** Button */
    TGTextButton       fButton;
    /** Layout hints */
    TGLayoutHints      fEntryHints;
    /** Number edit entry */
    TGNumberEntry      fEntry;
    /** Flag whether we've been set */
    Bool_t             fIsSet;  
    /** Colour  */
    Int_t fColor;
    ClassDef(Axis,0);
  };
  
  //__________________________________________________________________
  inline 
  Axis::Axis(TGCompositeFrame& left, const char* name, Int_t color)
    : TMarker(0,0,28), 
      fXpixel(-1), 
      fYpixel(-1), 
      fName(name),
      fFrameHints(kLHintsExpandX, 0, 0, 0, 3),
      fFrame(&left),
      fButtonHints(kLHintsLeft|kLHintsExpandX|kLHintsExpandY, 0, 3),
      fButton(&fFrame, fName.Data()),
      fEntryHints(kLHintsLeft|kLHintsExpandX|kLHintsExpandY, 3, 3, 0, 0),
      fEntry(&fFrame),
      fIsSet(kFALSE), 
      fColor(color)
  {
    SetMarkerSize(.7);
    SetMarkerColor(color);
    SetNDC();
    Pixel_t colour;
    gClient->GetColorByName((color == 3 ? "green" : 
			     (color == 4 ? "blue" : 
			      (color == 6 ? "magenta" : "cyan"))),colour);
    
    fButton.Connect("Clicked()", "DataStealer::Axis", this, "HandleButton()");
    fButton.AllowStayDown(kTRUE);
    fEntry.Connect("ValueSet(Long_t)","DataStealer::Axis",
		   this, "HandleChanged(Long_t)");
    fEntry.SetState(kFALSE);
    fEntry.GetNumberEntry()->SetTextColor(colour, kTRUE);
    
    fFrame.AddFrame(&fButton,     &fButtonHints);
    fFrame.AddFrame(&fEntry,      &fEntryHints);
    left.AddFrame(&fFrame, &fFrameHints);
  }
  //__________________________________________________________________
  inline void
  Axis::Clear(Option_t* option)
  {
    TMarker::Clear(option);
    gPad->RecursiveRemove(this);
    gPad->Modified();
    gPad->Update();
    gPad->cd();
    fIsSet = kFALSE;
    fXpixel = fYpixel = -1;
    fEntry.SetNumber(0);
    fEntry.SetState(kFALSE);
  }
  //__________________________________________________________________
  inline void
  Axis::Define(Axis*) 
  { 
    Long_t args[] = {(Long_t)this, 0};
    // Info("Define", "Emit signal");
    Emit("Define(DataStealer::Axis*)", args); 
  }
  //__________________________________________________________________
  inline void
  Axis::SetPixels(Int_t x, Int_t y)
  {
    fXpixel = x;
    fYpixel = y;
    fButton.Toggle();
    fEntry.SetState(kTRUE);
    fIsSet = kTRUE;
    SetX(Double_t(x) / gPad->GetWw());
    SetY(1 - Double_t(y) / gPad->GetWh());
    Draw();
    gPad->Modified();
    gPad->Update();
    gPad->cd();
  }
  //__________________________________________________________________
  inline void
  Axis::Focus() 
  { 
    fEntry.GetNumberEntry()->SetFocus(); 
    fEntry.GetNumberEntry()->SelectAll(); 
  }
}

#endif
//
// EOF
//

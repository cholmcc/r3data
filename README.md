# Read off data points and export to ROOT


This is a small GUI to read off data points from scanned or similar images

| Tilted image | With result | Logarithmic axis | Errors |
|--------------|-------------|------------------|--------|
| ![Screenshot](screenshot1.png) | ![Screenshot](screenshot2.png) | ![Screenshot](screenshot3.png) | ![Screenshot](screenshot4.png) |


## Usage

1. Load the program, by doing 

    ```
    Root> gSystem->Load("libASImage.so");
    Root> .L datastealer.C+
    Root> datastealer("my_image")
    ```

  The GUI shows the selected image file (png, jpeg, gif, ... what ever
  AfterImage supports). 

2. You then define the axis by setting 4 points. These are used to
   calculate the real values of the graph.  Logarithmic axis are
   supported. 

3. Next, you  can click on the image to set markers.   Markers can be
   deleted and moved at will.  A zoom of the image is shown on the left
   to help better position the markers. 

The list box on the right shows the coordinates (in
pixels and real values) of the currently defined points.  

You can add error bars to a point by selecting the entry in the list
box, and then click and drag on the image.  X and Y errors are
independent.

The read data can be exported to a ROOT macro (that defines a TGraph)
or a tab delimited ASCII file.  The graph, as it is currently defined,
can be shown too. 

## Standalone application 

Executing 

```
make
``` 

will build the standalone executable `datastealer`.  Use this as 

```
datastealer image-file
```
or with no argument to get a file selection dialong.

## Tests

The files (in the "test" subdirectory)

| Image     | Result  | Compare | Values      |
|-----------|---------|---------|-------------| 
| test1.png | test1.C | real1.C | values1.csv |
| test2.png | test2.C | real2.C | values2.csv |
| test3.png | test3.C | real3.C | values3.csv |

defines 3 test cases.  The files `testX.C` are the result of reading
off data from the images in `testX.png`.  The files `realX.C` draws
the real graphs, and `valuesX.csv` are ASCII files with the real data
points in them.   The script `test/compare.C` can be used to compare read
graphs to the real graphs. 

This code was heavily inspired by G3Data available from
http://www.frantz.fi/software/g3data.php.  

Christian Holm Christensen
<cholm@nbi.dk>

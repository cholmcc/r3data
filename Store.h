// -*- mode: C++ -*-
//
//  A class for grabbing data from scanned graphs
//  Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
// This code was heavily inspired by G3Data available from 
// http://www.frantz.fi/software/g3data.php.  The copyright of G3Data
// follows because some parts of this code is more or less lifted
// directly from there:
//
//  Copyright (C) 2000 Jonas Frantz <jonas.frantz@welho.com>
//
//  This file is part of g3data.
//
//  g3data is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  g3data is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef DATASTEALER_STORE
#define DATASTEALER_STORE
#include <TString.h>

namespace DataStealer 
{
  //====================================================================
  /** @class Store 
      @brief Store of Pixel/Value coordinate pairs. */
  struct Store
  {
    /** Constructor 
	@param px Pixel X coordinate 
	@param py Pixel Y coordinate 
	@param x  X coordinate 
	@param y  Y coordinate 
    */
    Store(Int_t    px=0, Int_t    py=0,
	  Double_t x =0, Double_t y =0);
    /** Destructor */
    virtual ~Store() {}
    /** Set pixel coordinates 
	@param px pixel X coordinate
	@param py pixel Y coordinate */
    void SetPixel(Int_t px=0, Int_t py=0);
    /** Set the value fields 
	@param x X coordinate 
	@param y Y coordinate */
    void SetValue(Double_t x=0, Double_t y=0);
    /** Clear the pixel and value fields */
    void ClearPixel();
    /** Clear the value fields */
    void ClearValue();
    /** Print to standard out */
    void Print() const;
    /** Get X (rounded) */
    Double_t X() const;
    /** Get Y (rounded) */
    Double_t Y() const;

    /** Pixel x--coordiante */
    Int_t    fXpixel;
    /** Pixtel y coordinate */
    Int_t    fYpixel;
    /** Real X value */
    Double_t fXvalue;
    /** Real Y value */
    Double_t fYvalue;
    /** X pixels as a string */
    TString  fXpstr;
    /** Y pixels as a string */
    TString  fYpstr;
    /** X value as a string */
    TString  fXvstr;
    /** Y value as a string */
    TString  fYvstr;

  };
  //__________________________________________________________________
  inline
  Store::Store(Int_t    px, Int_t    py,
	       Double_t x,  Double_t y) 
    : fXpixel(px), 
      fYpixel(py),
      fXvalue(x), 
      fYvalue(y),
      fXpstr(""),
      fYpstr(""),
      fXvstr(""),
      fYvstr("")
  {}
  //__________________________________________________________________
  inline void
  Store::SetPixel(Int_t px, Int_t py) 
  {
    fXpixel = px;
    fYpixel = py;
    fXpstr  = Form("%d", px);
    fYpstr  = Form("%d", py);
  }
  //__________________________________________________________________
  inline void
  Store::SetValue(Double_t x, Double_t y) 
  {
    fXvalue = x;
    fYvalue = y;
    fXvstr  = Form("%g", x);
    fYvstr  = Form("%g", y);
  }
  //__________________________________________________________________
  inline void
  Store::ClearPixel() 
  {
    fXpixel = -1;
    fYpixel = -1;
    fXpstr  = "";
    fYpstr  = "";
    ClearValue();
  }
  //__________________________________________________________________
  inline void
  Store::ClearValue() 
  {
    fXvalue = 0;
    fYvalue = 0;
    fXvstr  = "";
    fYvstr  = "";
  }
  //__________________________________________________________________
  inline void
  Store::Print() const 
  {
#if 0
    std::cout << "(" << std::setw(8) << fXvalue 
	      << "," << std::setw(8) << fYvalue << ") " 
	      << "[" << std::setw(8) << fXpixel 
	      << "," << std::setw(8) << fYpixel << "] "
	      << "(" << fXvstr << "," << fYvstr << ") "
	      << "[" << fXpstr << "," << fYpstr << "]" << std::endl;
#endif
  }
}
#endif /* DATASTEALER_STORE */
//
// EOF
//


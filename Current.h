// -*- mode: C++ -*-
//
//  A class for grabbing data from scanned graphs
//  Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
// This code was heavily inspired by G3Data available from 
// http://www.frantz.fi/software/g3data.php.  The copyright of G3Data
// follows because some parts of this code is more or less lifted
// directly from there:
//
//  Copyright (C) 2000 Jonas Frantz <jonas.frantz@welho.com>
//
//  This file is part of g3data.
//
//  g3data is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  g3data is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef DATASTEALER_CURRENT
#define DATASTEALER_CURRENT
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGTextEntry.h>

namespace DataStealer
{
  //====================================================================
  /** Display widget of current coordinate */
  struct Current : public TGHorizontalFrame
  {
    /** Constructor 
	@param f The frame to put this in.
	@param label Name of this coordinate */
    Current(TGCompositeFrame& f, const char* label);
    /** Set the value to display */
    void SetValue(Double_t v)
    {
      fValue.SetText(Form("%g", v));
      fError.SetText("");
    }
    void SetValue(Double_t v, Double_t e)
    {
      fValue.SetText(Form("%g", v));
      fError.SetText(Form("%g", e));
    }
    /** Destructor */
    virtual ~Current() {}
  protected:
    /** Hints for label and value */
    TGLayoutHints       fSubHints;
    /** Hints for label and value */
    TGLayoutHints       fLabHints;
    /** Hints for widget */
    TGLayoutHints       fHints;
    /** Label */
    TGLabel             fLabel;
    /** Display of coordinate */
    TGTextEntry         fValue;
    /** Label */
    TGLabel             fPlusMinus;
    /** Display of coordinate */
    TGTextEntry         fError;
    ClassDef(Current,0);
  };

  //__________________________________________________________________
  inline 
  Current::Current(TGCompositeFrame& f, const char* label)
    : TGHorizontalFrame(&f), 
      fSubHints(kLHintsExpandX, 1, 1, 0, 0),
      fLabHints(kLHintsNormal, 1, 1, 0, 0),
      fHints(kLHintsExpandX, 0, 0, 3, 0),
      fLabel(this, label), 
      fValue(this, ""),
      fPlusMinus(this, "+/-"),
      fError(this, "")
  {
    AddFrame(&fLabel,     &fLabHints);
    AddFrame(&fValue,     &fSubHints);
    AddFrame(&fPlusMinus, &fLabHints);
    AddFrame(&fError,     &fSubHints);
    f.AddFrame(this, &fHints);
    fValue.SetEnabled(kFALSE);
    fError.SetEnabled(kFALSE);
  }
}

#endif /* DATASTEALER_CURRENT */
//
// EOF
//

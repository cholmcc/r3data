// -*- mode: C++ -*-
//
//  A class for grabbing data from scanned graphs
//  Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
// This code was heavily inspired by G3Data available from 
// http://www.frantz.fi/software/g3data.php.  The copyright of G3Data
// follows because some parts of this code is more or less lifted
// directly from there:
//
//  Copyright (C) 2000 Jonas Frantz <jonas.frantz@welho.com>
//
//  This file is part of g3data.
//
//  g3data is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  g3data is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef DATASTEALER_MAIN
#define DATASTEALER_MAIN
#include <TGFrame.h>
#include <TGButton.h>
#include <TGListView.h>
#include <TGMenu.h>
#include <TGColorSelect.h>
#include <TGLabel.h>
#include <TGNumberEntry.h>
#include <TGStatusBar.h>
#include <TGFileDialog.h>
#include <TGMsgBox.h>
#include <TGedMarkerSelect.h>
#include <TGComboBox.h>
#include <TRootEmbeddedCanvas.h>
#include <TRootHelpDialog.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TTimer.h>
#include <TArrayD.h>
#include <TMath.h>
#include <TGraphAsymmErrors.h>
#include <TGraphErrors.h>
#include <TApplication.h>

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>

#include "Axis.h"
#include "Image.h"
#include "Current.h"
#include "Point.h"
#include "Calculator.h"

namespace DataStealer
{
  //====================================================================
  struct Main : public TGMainFrame
  {
    Main(const char* filename, Bool_t standAlone=kFALSE,Int_t zoomSize=200);
    virtual ~Main();
    /** Open a new image filee */
    Bool_t Open(const char* filename);
    /** Handle mouse in canvas */
    void   Handle(Int_t e,Int_t x,Int_t y,TObject* s);
    /** Handle menus */
    void   HandleMenu(Int_t id);
    /** Handle selection of point */
    void   HandleSelect(TGLVEntry* e, Int_t btn);
    /** Handle selection of point */
    void   HandleSelect();
    /** Handle request to calculate things */
    Bool_t HandleCalc();
    /** Handle request to calculate things */
    Bool_t HandleClear();
    /** Handle colour selection */ 
    void   HandleColor(Pixel_t pixel);
    /** Handle style selection */ 
    void   HandleStyle(Style_t s);
    /** Handle size selection */ 
    void   HandleSize(Long_t size);
    /** Handle output */
    void HandleOutput(Int_t id);
    /** Handle changes to calculation settings */
    void HandleChange();
    /** Define a reference point */
    void Define(Axis* a)
    {
      fAxisPoint = a;
      // Info("Define", "Settning current axis %p", fAxisPoint);
    }
    /** Test if all axis is defined */
    Bool_t IsAxisSet() const;
    /** Calculate the cache variables for speed */
    Bool_t CalcCommon();
    /** Close this window */
    void Close();

    /** Round value by precision (w/optional bias) 
	@param v  Value 
	@param e  Precision 
	@param o  Bias 
    */
    static Double_t Round(Double_t v, Double_t e, Int_t bias=0);
  protected:
    enum {
      kOpen, 
      kSave, 
      kSaveAs, 
      kDraw, 
      kPrint, 
      kExit, 
      kAbout, 
      kHelp
    };
    /** Draw the image */
    void DrawImage();
    /** Draw zoom around @f$ (x,y)@f$ 
	@param e Event
	@param x X-coordinate
	@param y Y-coordinate
	@param s Object. */
    void DoZoom(Int_t e,Int_t x,Int_t y,TObject* s);
    /** Make a graph */
    TGraph* MakeGraph(bool sym=false);

    /** Layout Hints */
    TGLayoutHints         fMenuHints;
    /** Menu bar */
    TGMenuBar             fMenu;
    /** File menu */
    TGPopupMenu*          fFileMenu;
    /** Help menu */
    TGPopupMenu*          fHelpMenu;

    /** Middle */ 
    TGLayoutHints         fMiddleHints;
    /** Middle */ 
    TGHorizontalFrame     fMiddle;

    /** Layout Hints */
    TGLayoutHints         fLeftHints;
    /** Layout Hints */
    TGLayoutHints         fMiscHints;
    /** Left hand frame */
    TGVerticalFrame       fLeft;
    /** Input widget for 1st X reference point */
    Axis                  fXLeft;
    /** Input widget for 2nd X reference point */
    Axis                  fXRight;
    /** Input widget for 1st Y reference point */
    Axis                  fYTop;
    /** Input widget for 2nd Y reference point */
    Axis                  fYBottom;
    /** Button to initiate calculations */
    TGTextButton          fCalc;
    /** Button to initiate calculations */
    TGTextButton          fClear;
    /** Current point group */
    TGGroupFrame          fCursor;
    /** Display widget to show cursor X value */
    Current               fXCursor;
    /** Display widget to show cursor Y value */
    Current               fYCursor;

    /** Log frame */
    TGGroupFrame          fLogFrame;
    /** Option button of Logarithmic X axis */
    TGCheckButton         fXLog;
    /** Option button of Logarithmic Y axis */
    TGCheckButton         fYLog;

    /** Log frame */
    TGGroupFrame          fRoundFrame;
    /** Option button of Logarithmic X axis */
    TGCheckButton         fRound;
    /** Label for rounding bias */
    TGLabel               fRoundBiasLabel;
    /** Option button of Logarithmic Y axis */
    TGNumberEntry         fRoundBias;
    
    /** Color Frame */
    TGGroupFrame          fMarkerFrame;
    /** Marker style selection */
    TGedMarkerSelect      fMarkerSelect;
    /** Marker size selection */
    TGNumberEntry         fMarkerSize;
    /** Color selector */ 
    TGColorSelect         fMarkerColor;
    
    /** Frame to hold zoom optons */
    TGGroupFrame          fZoomFrame;
    /** Label of zoom leve */
    TGLabel               fZoomLevelLabel;
    /** Layout Hints */
    TGNumberEntry         fZoomLevel;
    /** Label of zoom leve */
    TGLabel               fZoomRectLabel;
    /** Zoom level edit entry */
    TGNumberEntry         fZoomRect;
    /** Canvas to show zoom of image */
    TRootEmbeddedCanvas   fZoom;

    /** Output options */
    TGGroupFrame          fOutFrame;
    /** Format frame */
    TGHorizontalFrame     fOutFormatFrame;
    /** Format frame label */
    TGLabel               fOutFormatLabel;
    /** Format frame selection */
    TGComboBox            fOutFormat;
    /** Precision frame */
    TGHorizontalFrame     fOutPrecFrame;
    /** Precision label */
    TGLabel               fOutPrecLabel;
    /** Precision */
    TGNumberEntry         fOutPrec;
    /** Symmetrice uncertainties */
    TGCheckButton         fOutSym;
    
    /** Canvas to show image */
    TRootEmbeddedCanvas   fCanvas;

    /** Layout Hints */
    TGLayoutHints         fViewHints;
    /** View of points defined */
    TGListView            fView;
    /** Container of points defined */
    TGLVContainer         fPoints;

    /** Layout Hints */
    TGLayoutHints         fStatusHints;
    /** The status bar */
    TGStatusBar           fStatus;
  
    /** The image to read off */
    Image*                fImage;
    /** Pointer to reference point being defined - if any */
    Axis*                 fAxisPoint;
    /** The picked entry - if any */
    TGLVEntry*            fPoint;
    /** Zoom of image */
    Image*                fZoomImg;
    /** Width of zoom window */
    const Int_t           fZoomWidth;
    /** Height of zoom window */
    const Int_t           fZoomHeight;
    /** Current line */
    TLine* fLine;
    Int_t  fLineX;
    Int_t  fLineY;

    /** Cached variables OK */
    Bool_t                fCached;
    /** Our calculator */
    Calculator            fCalculator;
    /** Are we running as an application */
    Bool_t                fIsStandAlone;

    ClassDef(Main,0);
  };

  //====================================================================
  inline 
  Main::Main(const char* filename, Bool_t standAlone,
	     Int_t zoomSize) 
    : TGMainFrame    (gClient->GetRoot(), 1, 1, kVerticalFrame),
      fMenuHints     (kLHintsExpandX, 3, 3, 3, 2),
      fMenu          (this),
      fMiddleHints   (kLHintsExpandX|kLHintsExpandY, 3, 3, 3, 3),
      fMiddle        (this),
      fLeftHints     (kLHintsExpandX, 3, 3, 3, 3),
      fMiscHints     (kLHintsExpandX, 3, 3, 3, 1),
      fLeft          (&fMiddle, 200, 200),
      fXLeft         (fLeft,           "Define X1", 3),
      fXRight        (fLeft,           "Define X2", 4),
      fYTop          (fLeft,           "Define Y1", 6),
      fYBottom       (fLeft,           "Define Y2", 7),
      fCalc          (&fLeft,          "Calculate"),
      fClear         (&fLeft,          "Clear"),
      fCursor        (&fLeft,          "Cursor"),
      fXCursor       (fCursor,         "X:"),
      fYCursor       (fCursor,         "Y:"),
      fLogFrame      (&fLeft,          "Logarithmic scales", kHorizontalFrame),
      fXLog          (&fLogFrame,      "X"),
      fYLog          (&fLogFrame,      "Y"),
      fRoundFrame    (&fLeft,          "Rounding",kHorizontalFrame),
      fRound         (&fRoundFrame,    " "),
      fRoundBiasLabel(&fRoundFrame,    "Bias"),
      fRoundBias     (&fRoundFrame,    0, 0, -1,
		      TGNumberFormat::kNESInteger, 
		      TGNumberFormat::kNEAAnyNumber,
		      TGNumberFormat::kNELLimitMinMax, 
		      -16, 16),		      
      fMarkerFrame   (&fLeft,          "Marker", kHorizontalFrame),
      fMarkerSelect  (&fMarkerFrame, 25, -1),
      fMarkerSize    (&fMarkerFrame,2,4,-1,
		      TGNumberFormat::kNESRealOne,
		      TGNumberFormat::kNEANonNegative,
		      TGNumberFormat::kNELLimitMinMax, 0.2, 5.0),
      fMarkerColor   (&fMarkerFrame, 2),
      fZoomFrame     (&fLeft,          "Zoom",kHorizontalFrame),
      fZoomLevelLabel(&fZoomFrame,     "Level:"),
      fZoomLevel     (&fZoomFrame, 4, 0, -1,
		      TGNumberFormat::kNESInteger, 
		      TGNumberFormat::kNEAPositive,
		      TGNumberFormat::kNELLimitMinMax, 
		      2, 8),
      fZoomRectLabel (&fZoomFrame,     "Reticle:"),
      fZoomRect      (&fZoomFrame, 4, 0, -1,
		      TGNumberFormat::kNESInteger, 
		      TGNumberFormat::kNEAPositive,
		      TGNumberFormat::kNELLimitMinMax, 
		      2, 20),
      fZoom          ("zoom", &fLeft, zoomSize, zoomSize),
      fOutFrame      (&fLeft,           "Output"),
      fOutFormatFrame(&fOutFrame),
      fOutFormatLabel(&fOutFormatFrame, "Format"),
      fOutFormat     (&fOutFormatFrame, "scientific", 2),
      fOutPrecFrame  (&fOutFrame),
      fOutPrecLabel  (&fOutPrecFrame,   "Precision"),
      fOutPrec       (&fOutPrecFrame, -1, 0, -1,
		      TGNumberFormat::kNESInteger, 
		      TGNumberFormat::kNEAAnyNumber,
		      TGNumberFormat::kNELLimitMinMax, 
		      -1, 16),
      fOutSym        (&fOutFrame,       "Sym.Uncer."),
      fCanvas        ("c", &fMiddle, 100, 100),
      fViewHints     (kLHintsExpandY|kLHintsExpandX, 3, 3, 1, 3),
      fView          (&fMiddle, 100, 100),
      fPoints        (&fView),
      fStatusHints   (kLHintsExpandX, 0, 0, 0, 3),
      fStatus        (this),
      fImage         (0),
      fAxisPoint     (0), 
      fPoint         (0),
      fZoomImg       (0),
      fZoomWidth     (200),
      fZoomHeight    (200),
      fLine          (0),
      fIsStandAlone  (standAlone)
  {
    // Menu bar 
    fFileMenu = fMenu.AddPopup("&File");
    fFileMenu->AddSeparator();
    fFileMenu->AddEntry("&Open",       kOpen);
    fFileMenu->AddEntry("&Save",       kSave);
    fFileMenu->AddEntry("Save &As...", kSaveAs);
    fFileMenu->AddSeparator();
    fFileMenu->AddEntry("&Draw",       kDraw);
    fFileMenu->AddEntry("&Print",      kPrint);
    fFileMenu->AddSeparator();
    fFileMenu->AddEntry("E&xit...",    kExit);  
    fFileMenu->Connect("Activated(Int_t)", "DataStealer::Main", 
		       this, "HandleMenu(Int_t)");
    fHelpMenu = fMenu.AddPopup("&Help");
    fHelpMenu->AddEntry("&About ...", kAbout);
    fHelpMenu->AddEntry("Help ...", kHelp);
    fHelpMenu->Connect("Activated(Int_t)", "DataStealer::Main", 
		       this, "HandleMenu(Int_t)");
    AddFrame(&fMenu, &fMenuHints);


    // Top frame with canvas and operations. 
    AddFrame(&fMiddle, &fMiddleHints);
    fMiddle.AddFrame(&fLeft, new TGLayoutHints(0, 0, 3, 0, 0));
  
    // Defines 
    fXLeft  .Connect("Define(DataStealer::Axis*)",
		     "DataStealer::Main",this,
		     "Define(DataStealer::Axis*)");
    fXRight .Connect("Define(DataStealer::Axis*)",
		     "DataStealer::Main",this,
		     "Define(DataStealer::Axis*)");
    fYTop   .Connect("Define(DataStealer::Axis*)",
		     "DataStealer::Main",this,
		     "Define(DataStealer::Axis*)"); 
    fYBottom.Connect("Define(DataStealer::Axis*)",
		     "DataStealer::Main",this,
		     "Define(DataStealer::Axis*)");
    fXLeft  .Connect("Changed()",  "DataStealer::Main", this, "HandleChange()");
    fXRight .Connect("Changed()",  "DataStealer::Main", this, "HandleChange()");
    fYTop   .Connect("Changed()",  "DataStealer::Main", this, "HandleChange()");
    fYBottom.Connect("Changed()",  "DataStealer::Main", this, "HandleChange()");

    // Calculation button 
    fCalc .Connect("Clicked()", "DataStealer::Main", this, "HandleCalc()");
    fClear.Connect("Clicked()", "DataStealer::Main", this, "HandleClear()");
    fCalc .SetToolTipText("(Re)Calculate values of points");
    fClear.SetToolTipText("Clear away all points (not axes definitions)");
    fCalc .SetEnabled(kFALSE);
    fClear.SetEnabled(kTRUE);
    fLeft .AddFrame(&fCalc,  &fLeftHints);
    fLeft .AddFrame(&fClear, &fLeftHints);

    // Cursor
    fLeft .AddFrame(&fCursor, &fLeftHints);
    
    // Logarithmic options    
    fXLog.Connect("Clicked()", "DataStealer::Main",this,"HandleChange()");
    fYLog.Connect("Clicked()", "DataStealer::Main",this,"HandleChange()");
    fXLog.SetToolTipText("Set logarithmic abscissa");
    fYLog.SetToolTipText("Set logarithmic ordinate");
    fLogFrame.AddFrame(&fXLog, &fMiscHints);
    fLogFrame.AddFrame(&fYLog, &fMiscHints);
    fLeft.AddFrame(&fLogFrame, &fMiscHints);

    // Rounding
    fRound    .Connect("Clicked()", "DataStealer::Main",this,"HandleChange()");
    fRoundBias.Connect("ValueSet(Long_t)", "DataStealer::Main",
		       this, "HandleChange()");
    fRoundFrame.AddFrame(&fRound,          &fMiscHints);
    fRoundFrame.AddFrame(&fRoundBiasLabel, &fMiscHints);
    fRoundFrame.AddFrame(&fRoundBias,      &fMiscHints);
    fLeft.AddFrame(&fRoundFrame, &fLeftHints);
  
    // Marker
    fMarkerSize.GetNumberEntry()->SetToolTipText("Set marker size");
    fMarkerSelect.SetToolTipText("Set marker style");
    fMarkerColor.SetToolTipText("Set marker and rectile colour");
    fMarkerSelect.Connect("MarkerSelected(Style_t)", "DataStealer::Main",
    			  this, "HandleStyle(Style_t)");
    fMarkerSize.Connect("ValueSet(Long_t)", "DataStealer::Main",
    			this, "HandleSize(Long_t)");
    fMarkerColor.Connect("ColorSelected(Pixel_t)","DataStealer::Main",
		   this, "HandleColor(Pixel_t)");
    fMarkerColor.SetColor(TColor::RGB2Pixel(255,0,0),kFALSE);
    fMarkerFrame.AddFrame(&fMarkerColor,  &fMiscHints);
    fMarkerFrame.AddFrame(&fMarkerSelect, &fMiscHints);
    fMarkerFrame.AddFrame(&fMarkerSize,   &fMiscHints);
    fLeft       .AddFrame(&fMarkerFrame,  &fMiscHints);
  
    // Zoom operations 
    // fZoomOpt = new TGHorizontalFrame(fLeft);
    fZoomLevel.GetNumberEntry()->SetToolTipText("Level of zoom");
    fZoomRect.GetNumberEntry()->SetToolTipText("Rectile size in zoom");
    fZoomFrame.AddFrame(&fZoomLevelLabel, &fMiscHints);
    fZoomFrame.AddFrame(&fZoomLevel,      &fMiscHints);
    fZoomFrame.AddFrame(&fZoomRectLabel , &fMiscHints);
    fZoomFrame.AddFrame(&fZoomRect,       &fMiscHints);
    fLeft     .AddFrame(&fZoomFrame,      &fMiscHints);
  

    // Zoom canvas 
    // fZoom = new TRootEmbeddedCanvas("zoom", &fLeft, 200, 200),
    fZoom.GetCanvas()->SetBit(TPad::kClearAfterCR, kFALSE);
    fZoom.GetCanvas()->SetBorderMode(0);
    fLeft.AddFrame(&fZoom, &fMiscHints);

    // Output optins
    // fOutFormat.GetTextEntry()->SetToolTipText("Float formatting in CSV");
    fOutPrec  .GetNumberEntry()->SetToolTipText("Precision (-1: default)");
    fOutSym   .SetToolTipText("Make uncertainties symmetric");
    fOutFormat     .AddEntry("default",   0);
    fOutFormat     .AddEntry("fixed",     1);
    fOutFormat     .AddEntry("scientific",2);
    fOutFrame      .AddFrame(&fOutFormatFrame, &fMiscHints);
    fOutFrame      .AddFrame(&fOutPrecFrame,   &fMiscHints);
    fOutFormatFrame.AddFrame(&fOutFormatLabel, &fMiscHints);
    fOutFormatFrame.AddFrame(&fOutFormat,      &fMiscHints);
    fOutPrecFrame  .AddFrame(&fOutPrecLabel,   &fMiscHints);
    fOutPrecFrame  .AddFrame(&fOutPrec,        &fMiscHints);
    fOutFrame      .AddFrame(&fOutSym,         &fMiscHints);
    fLeft          .AddFrame(&fOutFrame,       &fMiscHints);
    
    // Canvas to display the image in 
    // fCanvas = new TRootEmbeddedCanvas("c", &fMiddle, 100, 100),
    fCanvas.GetCanvas()->SetBit(TPad::kClearAfterCR, kFALSE);
    fCanvas.GetCanvas()->SetBorderMode(0);
    fCanvas.GetCanvas()->Connect("ProcessedEvent(Int_t,Int_t,Int_t,TObject*)",
				 "DataStealer::Main", this, 
				 "Handle(Int_t,Int_t,Int_t,TObject*)");
    fMiddle.AddFrame(&fCanvas);

    // View of points defined 
    fView.SetContainer(&fPoints);
    fView.SetHeaders(9);
    fView.SetHeader("#",             kTextLeft,    kTextLeft,    0);
    fView.SetHeader("Xpixel",        kTextCenterX, kTextCenterX, 1);
    fView.SetHeader("Ypixel",        kTextCenterX, kTextCenterX, 2);
    fView.SetHeader("X value",       kTextCenterX, kTextCenterX, 3);
    fView.SetHeader("Y value",       kTextCenterX, kTextCenterX, 4);
    fView.SetHeader("X low error",   kTextCenterX, kTextCenterX, 5);
    fView.SetHeader("X high error",  kTextCenterX, kTextCenterX, 6);
    fView.SetHeader("Y low error",   kTextCenterX, kTextCenterX, 7);
    fView.SetHeader("Y high error",  kTextCenterX, kTextCenterX, 8);
    fView.Connect("Clicked(TGLVEntry*,Int_t)", "DataStealer::Main", this, 
		  "HandleSelect(TGLVEntry*,Int_t)");
    fView.Connect("SelectionChanged()", "DataStealer::Main", this, 
		  "HandleSelect()");
    // fPoints.SetViewMode(kLVDetails);
    fPoints.SetListView(&fView);
    fPoints.SetLayoutManager(new TGColumnLayout(&fPoints));
    fPoints.Associate(&fView);
    fMiddle.AddFrame(&fView, &fViewHints);
    Pixel_t white;
    gClient->GetColorByName("white",white);
    fPoints.SetBackgroundColor(white);
    fView.SetViewMode(kLVDetails);
    fView.SetScrolling(TGCanvas::kCanvasScrollBoth);

    // Status bar 
    fStatus.SetParts(1);
    fStatus.SetText("Left click to add point");
    AddFrame(&fStatus, &fStatusHints);
  
    // Connect close button
    Connect("CloseWindow()", "DataStealer::Main", this, "Close()");

    Open(filename);
  }
  //____________________________________________________________________
  inline Main::~Main() 
  {
    fCanvas.GetCanvas()->Clear();
    fZoom.GetCanvas()->Clear();
    if (fImage) delete fImage;
    if (fZoomImg) delete fZoomImg;
  }
  //____________________________________________________________________
  inline void 
  Main::Close()
  {
    // DontCallClose();
    // DestroyWindow();
    DontCallClose();
    TTimer::SingleShot(150, "DataStealer::Main", this, "DeleteWindow()");
    if (!fIsStandAlone) return;
    gApplication->Terminate();
  }
  //____________________________________________________________________
  inline Bool_t
  Main::Open(const char* filename) 
  {
    // Initialize the images. 
    if (fImage) delete fImage;
    fCanvas.GetCanvas()->Clear("d");
  
    static const char* types[] = {
      "Portable Network Graphics",	 "*.png", 
      "Tagged Image File Format",	 "*.tif", 
      "Graphics Interchange Format",	 "*.gif", 
      "Joint Photographic Experts Group","*.jpg", 
      "Portable Pixmap Format",		 "*.ppm", 
      "Portable anymap",		 "*.pnm", 
      "eXperimental Computing Facility", "*.xcf", 
      "Windows Bitmap",			 "*.bmp", 
      "X BitMap",			 "*.xbm", 
      "X PixMap",			 "*.xpm", 
      "Compressed X PixMap",		 "*.xpm.Z", 
      "GZip Compressed X PixMap",	 "*.xpm.gz", 
      "Microsoft Icon",			 "*.ico", 
      "Microsoft Cursor Icon",		 "*.cur", 
      "Flexible Image Transport System", "*.fits", 
      "Truevision TGA",			 "*.tga", 
      "XML",				 "*.xml", 
      "All",				 "*.*", 
      0
    };
    Int_t type = 34;
    TString fname(filename);
    while (fname.IsNull()) {
      TGFileInfo info;
      info.fFileTypes = types;
      info.fFileTypeIdx = 0;
      new TGFileDialog(gClient->GetRoot(), this, kFDOpen, &info);
      fname = info.fFilename;
      type  = info.fFileTypeIdx;
      if (fname.IsNull()) {
	Int_t ret;
	new TGMsgBox(gClient->GetRoot(), this, 
		     // (this == nullptr ? this : gClient->GetRoot()),
		     "Please specify a file", 
		     "No file given, press OK to continue and pick a file", 
		     kMBIconAsterisk, kMBOk|kMBCancel, &ret);
	if (ret == kMBCancel) throw "Arg!";
	continue;
      }
    }
    if (type != 34 && !fname.EndsWith(&((types[type + 1])[1]))) 
      fname.Append(&((types[type + 1])[1]));

    fImage    = new Image(fname.Data());
    if (!fImage) throw "Argh!";
    fZoomImg  = 0; 
    Int_t iw  = fImage->GetWidth();
    Int_t ih  = fImage->GetHeight();
    Int_t vw  = 200;
    Int_t tw  = iw + fZoomWidth+vw;
    Int_t vh  = 0; // 100;
    Int_t th  = ih + vh;

    // Resize(tw, th);
    Resize(tw, th);
    fMiddle.Resize(tw, ih);
    fLeft.Resize(fZoomWidth, ih + 10);
    fView.Resize(vw, ih); // tw, vh);
    fCanvas.Resize(iw, ih);
    // fPoints->RemoveAll();

    // Show it 
    MapSubwindows();
    Resize(GetDefaultSize());
    Int_t w = int(GetWidth());
    Int_t h = int(GetHeight());
    SetWMSize(w, h);
    SetWMSizeHints(w, int(h*.8), 2*w, 2*h, 2, 2);
    SetWindowName("R3Data");
    SetIconName("R3Data");
    SetClassHints("R3Data", "R3Data");
    MapWindow();
  
    DrawImage();
    return kTRUE;
  }
  //____________________________________________________________________
  inline Bool_t
  Main::IsAxisSet() const
  {
    return (fXLeft.IsSet() && fXRight.IsSet() && 
	    fYTop.IsSet() && fYBottom.IsSet());
  }
  //____________________________________________________________________
  inline void 
  Main::DoZoom(Int_t, Int_t x, Int_t y, TObject*) 
  {
    Int_t ix1, iy1, ix2, iy2;

    // Find Zoom size, and create image 
    Int_t zw = fZoomWidth  / fZoomLevel.GetIntNumber();
    Int_t zh = fZoomHeight / fZoomLevel.GetIntNumber();
    Int_t zr = fZoomRect.GetIntNumber();
    if (fZoomImg) delete fZoomImg;
    fZoomImg = new Image(zw, zh);

    // Find section of main image and copy to zoom 
    fImage->PadToImage(x - zw / 2, y - zh / 2, ix1, iy1);
    fImage->PadToImage(x + zw / 2, y + zh / 2, ix2, iy2);
    fImage->CopyArea(fZoomImg, ix1, iy1, ix2-ix1, iy2-iy1,
		     0, 0, 3, TImage::kAllChan);
    Int_t r, g, b;
    TColor::Pixel2RGB(fMarkerColor.GetColor(),r,g,b);
    unsigned src = ((r & 0xFF) << 16) | ((g & 0xFF) << 8) | (b & 0xFF);
    // Printf("%06x %s",src, TColor::PixelAsHexString(fColor.GetColor()));
    TArrayD* pixels = fZoomImg->GetArray();

    auto get = [pixels,zh,zw,src](Int_t x, Int_t y) {
      double   dv = pixels->fArray[(zh-y-1)*zw+x];
      unsigned pv = unsigned(0xFFFFFF * dv);
      return src ^ pv;
    };
      
    char col[8];
    // Vertical line
    for (Int_t i = 0; i < zh; i++) {
      if (i == zh/2 || i == zh/2-1) continue;
      sprintf(col, "#%06x", get(zw/2,i));
      fZoomImg->DrawBox(zw/2,i,zw/2,i+1,col,1,1);
    }
    // Horizontal line
    for (Int_t i = 0; i < zw; i++) {
      if (i == zw/2 || i == zw/2-1) continue;
      sprintf(col, "#%06x", get(i,zh/2));
      fZoomImg->DrawBox(i,zh/2,i+1,zh/2,col,1,1);
    }

    // Recticule vertical
    Int_t ryl = zh/2-zr-1;
    Int_t ryh = zh/2+zr+1;
    Int_t rxl = zw/2-zr-1;
    Int_t rxh = zw/2+zr+1;
    for (Int_t i = ryl; i < ryh; i++) {
      sprintf(col, "#%06x", get(rxl,i));
      fZoomImg->DrawBox(rxl,i,rxl,i+1,col,1,1);
      sprintf(col, "#%06x", get(rxh,i));
      fZoomImg->DrawBox(rxh,i,rxh,i+1,col,1,1);
    }
    // Recticule horizontal
    for (Int_t i = rxl; i < rxh; i++) {
      sprintf(col, "#%06x", get(i,ryl));
      fZoomImg->DrawBox(i,ryl,i+1,ryl,col,1,1);
      sprintf(col, "#%06x", get(i,ryh));
      fZoomImg->DrawBox(i,ryh,i+1,ryh,col,1,1);
    }
    
    delete pixels;
    
    fZoomImg->EndPaint();
    TCanvas* zoomCanvas = fZoom.GetCanvas();
    zoomCanvas->SetLeftMargin(0);
    zoomCanvas->SetRightMargin(0);
    zoomCanvas->SetTopMargin(0);
    zoomCanvas->SetBottomMargin(0);    
    zoomCanvas->Clear();
    zoomCanvas->Modified(kTRUE);
    zoomCanvas->Update();
    zoomCanvas->cd();
    fZoomImg->Draw();
    zoomCanvas->Modified(kTRUE);
    zoomCanvas->Update();
    zoomCanvas->cd();
    gClient->NeedRedraw(&fZoom, kTRUE);
  }  
  //____________________________________________________________________
  inline void 
  Main::Handle(Int_t e,Int_t x,Int_t y, TObject* o) 
  {
    static int shift = 0;
    
    // Update the zoom 
    DoZoom(e, x, y, o);

    // Go to main canvas 
    fCanvas.GetCanvas()->cd();

    // Possibly cache variable
    CalcCommon();

    // Update position display 
    if (IsAxisSet()) {
      Double_t xx,yy,ex,ey;
      fCalculator.Pixel2Value(x,y, xx, yy, ex, ey);
      fXCursor.SetValue(xx,ex);
      fYCursor.SetValue(yy,ey);
    }

    // Handle different events 
    switch (e) {
    case kArrowKeyRelease:
      // Update point?
      // Info("Handle", "arrow keys");
      if (fPoint) {
	Point* p = static_cast<Point*>(fPoint->GetUserData());
	if (!p) return;
	
	if (!IsAxisSet()) {
	  p->SetPixels(x, y);
	  return;
	}
	fCalculator.Pixel2Value(x, y, p);
	return;
      }

      // Update axis point 
      if (fAxisPoint) {
	fAxisPoint->SetPixels(x, y);
	fCalc.SetEnabled(IsAxisSet());
	fCached = false;
	CalcCommon();
	return;
      }
      break;

    case kButton1Shift:
      // Info("Handle", "S-Mouse 1");
      if (!fPoint) {
	// Info("Handle", "No current point on shift-click");
	Handle(kButton1Down,x,y,o);
	return;
      }
      if (fLine) {
	Handle(kButton1Up,x,y,o);
	return;
      }
      // Info("Handle", "Starting error on shift-click");
      Handle(kButton1Down,x,y,o);
      Handle(kButton1Motion,x,y,o);
      shift = 2;
      break;
      
    case kButton1Down: 
      // Info("Handle", "Mouse 1 Down");
      // gVirtualX->SetLineColor(-1);
      if (shift <= 0) {
	fLineX = x;
	fLineY = y;
      }
      break;

    case kMouseMotion:
      if (shift <= 0) return;
      // Info("Handle","Mouse motion while error line");
      // Fall through
    case kButton1Motion:
      // Info("Handle", "Mouse motion or drag");
      // If we have a point, we're doing error so draw line 
      if (fPoint) {
	// Info("Handle", "Updating error line (%d,%d)",fLineX,fLineY);
	Double_t w = gPad->GetWw();
	Double_t h = gPad->GetWh();
	if (!fLine) {
	  fLine = new TLine(double(fLineX)/w,1-double(fLineY)/h,0,0);
	  fLine->SetBit(TLine::kLineNDC);
	  fLine->SetLineColor(4);
	}
	fLine->SetX2(double(x)/w);
	fLine->SetY2(1-double(y)/h);
	fCanvas.GetCanvas()->cd();
	fLine->Draw();
	gPad->Modified();
	gPad->Update();
	gPad->cd();
      }
      break;

    case kButton1Up:
      // Info("Handle", "Mouse 1 Up");
      if (--shift > 0) return;
      
      fCanvas.GetCanvas()->cd();

      // If we have a line it's an error being defined 
      if (fLine) {
	delete fLine;
	fLine = 0;

	if (!fPoint) {
	  std::cout << "Argh! no entry selected!" << std::endl;
	  return;
	}

	Point* p = static_cast<Point*>(fPoint->GetUserData());
	if (!p) return;

	Double_t dx = TMath::Abs(x - fLineX);
	Double_t dy = TMath::Abs(y - fLineY);
	Store&   e1 = (dx > dy ? 
		       (fLineX < x ? p->fEX1 : p->fEX2) : 
		       (fLineY < y ? p->fEY2 : p->fEY1));
	Store&   e2 = (dx > dy ? 
		       (fLineX < x ? p->fEX2 : p->fEX1) : 
		       (fLineY < y ? p->fEY1 : p->fEY2));
	e1.SetPixel(fLineX, fLineY);
	e2.SetPixel(x, y);
	
	if (!IsAxisSet()) { 
	  p->Update();
	  return;
	}

	Double_t xv = p->fPos.fXvalue;
	Double_t yv = p->fPos.fYvalue;
	Double_t e1X, e1Y, e2X, e2Y;
	fCalculator.Pixel2Error(e1.fXpixel, e1.fYpixel, xv, yv, e1X, e1Y);
	fCalculator.Pixel2Error(e2.fXpixel, e2.fYpixel, xv, yv, e2X, e2Y);
	e1.SetValue(e1X, e1Y);
	e2.SetValue(e2X, e2Y);
	p->Update();
	return;
      } // Error - fLine not null

      // Check if we're moving an axis point
      if (fAxisPoint) {
	fAxisPoint->SetMarkerSize(fMarkerSize.GetNumber());
	fAxisPoint->SetPixels(x, y);
	fAxisPoint->Focus();
	fAxisPoint = 0;
	fCalc.SetEnabled(IsAxisSet());
	CalcCommon();
	return;
      }

      // Other wise, new point or moving point
      {
	Int_t        n = fPoints.GetList()->GetEntries();
	Point*       p = 0;
	if (!fPoint) p = new Point(fPoints, n);
	else         p = static_cast<Point*>(fPoint->GetUserData());
	if (!p)      return;
	
	// Now set point attributes
	fCanvas.GetCanvas()->cd();
	p->SetColor(TColor::GetColor(fMarkerColor.GetColor()));
	p->SetStyle(fMarkerSelect.GetMarkerStyle());
	p->SetSize(fMarkerSize.GetNumber());
	
	// If axes are not defined, store pixel values 
	if (!IsAxisSet()) { 
	  p->SetPixels(x, y);
	  return;
	}

	// Otherwise caculate value
	fCalculator.Pixel2Value(x, y, p);
      }
      break;
    case kButton3Down: 
      break;
    case kButton3Up: 
      break;
    }  
  }

  //____________________________________________________________________
  inline void 
  Main::HandleColor(Pixel_t pixel)
  {
    TIter            next(fPoints.GetList());
    const TGLVEntry* e    = 0;
    TGFrameElement*  fe   = 0;
    Point*           p    = 0;
    while ((fe = static_cast<TGFrameElement*>(next()))) {
      e = static_cast<const TGLVEntry*>(fe->fFrame);
      if (!e) continue;
      p = static_cast<Point*>(e->GetUserData());
      if (!p) continue;
      p->SetColor(TColor::GetColor(pixel));
    }
  }
  //____________________________________________________________________
  inline void 
  Main::HandleStyle(Style_t style)
  {
    TIter            next(fPoints.GetList());
    const TGLVEntry* e    = 0;
    TGFrameElement*  fe   = 0;
    Point*           p    = 0;
    while ((fe = static_cast<TGFrameElement*>(next()))) {
      e = static_cast<const TGLVEntry*>(fe->fFrame);
      if (!e) continue;
      p = static_cast<Point*>(e->GetUserData());
      if (!p) continue;
      p->SetStyle(style);
    }
  }
  //____________________________________________________________________
  inline void 
  Main::HandleSize(Long_t size)
  {
    TIter            next(fPoints.GetList());
    const TGLVEntry* e    = 0;
    TGFrameElement*  fe   = 0;
    Point*           p    = 0;
    while ((fe = static_cast<TGFrameElement*>(next()))) {
      e = static_cast<const TGLVEntry*>(fe->fFrame);
      if (!e) continue;
      p = static_cast<Point*>(e->GetUserData());
      if (!p) continue;
      p->SetSize(fMarkerSize.GetNumber());
    }
    fXLeft  .SetMarkerSize(fMarkerSize.GetNumber());
    fXRight .SetMarkerSize(fMarkerSize.GetNumber());
    fYTop   .SetMarkerSize(fMarkerSize.GetNumber());
    fYBottom.SetMarkerSize(fMarkerSize.GetNumber());
  }

  //____________________________________________________________________
  inline void 
  Main::HandleMenu(Int_t id)
  {
    if (id == kExit) {
      Close();
      // SendCloseMessage();
      // delete this;
      return;
    }
    if (id == kOpen) {
      Open(0);
      return;
    }
    if (id == kAbout) {
      new TGMsgBox(gClient->GetRoot(), this, "About DataStealer", 
		   "Program to read data from images\n"
		   "Copyright (C) 2007 Christian Holm\n"
		   "Released under the GPL version 2\n"
		   "Heavily inspired by G3Data");
      return;
    }
    if (id == kHelp) {
      TRootHelpDialog* hd = new TRootHelpDialog(this, "Help on DataStealer", 
						600, 400);
      static const char* helpText = 
	"Program to read data from images\n\n"
	"Define x and y axis by pressing buttons on the left, click in\n"
	"image, and fill in the appropriate coordinate value.\n\n"
	"Define data points by clicking on the image. Use the zoom on the\n"
	"left for better precision.\n\n"
	"By selecting entries in the right panel, the data points can be\n"
	"edited or removed.\n\n"
	"To define error bars, select an item in the lower panel, and drag\n"
	"mouse over error bars while holding down the left mouse button.\n\n"
	"Alternatively, start error bar by shift-left mouse button, and\n"
	"finish with left mouse button.\n\n"
	"Select a point on the right and use arrow keys to fine-tune\n"
	"the location of the point\n\n"
	"Choose 'File->Draw' to see the graph. Choose 'File->Save' or\n"
	"'File->Save As ...' to save to a script or an ASCII file\n"
	"Options on the left determine how those files are written\n";
      hd->SetText(helpText);
      hd->Popup();
      return;
    }

    HandleOutput(id);
  }
  //____________________________________________________________________
  inline TGraph*
  Main::MakeGraph(bool sym)
  {
    if (!HandleCalc()) return 0;

    Bool_t hasErrors = kFALSE;
    const TGLVEntry* e      = 0;
    TGFrameElement*  fe     = 0;
    Point*           p      = 0;
    TIter            next(fPoints.GetList());
    while ((fe = static_cast<TGFrameElement*>(next()))) {
      e = static_cast<const TGLVEntry*>(fe->fFrame);
      if (!e) continue;
      p = static_cast<Point*>(e->GetUserData());
      if (!p) continue;
      if (p->HasXError() || p->HasYError()) {
	hasErrors = kTRUE;
	break;
      }
    }
    TGraph*            g = 0;
    TGraphAsymmErrors* a = 0;
    TGraphErrors*      s = 0;
    if (hasErrors) {
      if (!sym)
	g = a = new TGraphAsymmErrors;
      else
	g = s = new TGraphErrors;
    }
    else
      g = new TGraph;

    g->SetMarkerStyle(21);
    g->SetMarkerSize(.6);
    g->SetName("graph");
    g->SetTitle("Data read using DataStealer");
    Int_t i = 0;
    next.Reset();
    while ((fe = static_cast<TGFrameElement*>(next()))) {
      // (p = static_cast<Point*>(e->GetUserData())w)) {
      e = static_cast<const TGLVEntry*>(fe->fFrame);
      if (!e) continue;
      p = static_cast<Point*>(e->GetUserData());
      if (!p) continue;
      g->SetPoint(i, p->fPos.fXvalue, p->fPos.fYvalue);
      // g->SetPointError(i, p->fXerr, p->Yerr);
      if (p->HasXError() || p->HasYError()) {
	if (a)  
	  a->SetPointError(i, 
			   p->fEX1.fXvalue, p->fEX2.fXvalue, 
			   p->fEY1.fYvalue, p->fEY2.fYvalue);
	else if (s) 
	  s->SetPointError(i,
			   (p->fEX1.fXvalue + p->fEX2.fXvalue) / 2,
			   (p->fEY1.fYvalue + p->fEY2.fYvalue) / 2);
      }
      i++;
    }
    return g;
  }
  
  //____________________________________________________________________
  inline void
  Main::HandleOutput(Int_t id)
  {
    TGraph*            g = MakeGraph(fOutSym.IsOn());
    TGraphAsymmErrors* a = dynamic_cast<TGraphAsymmErrors*>(g);
    TGraphErrors*      s = dynamic_cast<TGraphErrors*>(g);
    if (!g) return;
    
    if (id == kDraw) {
      TCanvas* c = new TCanvas("graph", "Graph");
      c->SetLogy(fCalculator.fY.fIsLog);
      c->SetLogx(fCalculator.fX.fIsLog);
      g->Draw("APL");
      return;
    }
    if (id == kPrint) {
      g->Print();
      return;
    }

    // The rest of this member function is for saving 
    if (id != kSaveAs) return;
    
    // Type of file to save as 
    static const char* types[] = { "ROOT script", "*.C", 
				   "Plain data file", "*.csv", 
				   0 };

    // Default name 
    TString fname("graph.C");
    Int_t   type = 0;

    // We open a file dialog 
    TGFileInfo info;
    info.fFileTypes = types;
    // info.fFilename  = fname.Data();
    new TGFileDialog(gClient->GetRoot(), this, kFDSave, &info);
    if (!info.fFilename) return;
    
    // Get the selected filename and type 
    fname  = info.fFilename;
    type   = info.fFileTypeIdx;
    if (fname.IsNull()) { 
      new TGMsgBox(gClient->GetRoot(), this, "Failed", 
		   "No file name specified", kMBIconExclamation);
      return;
    }

    // Append ending if not given 
    if (!fname.EndsWith(&((types[type+1])[1]))) 
      fname.Append(&((types[type+1])[1]));

    // Open the output file 
    std::ofstream out(fname.Data());
    if (!out) {
      new TGMsgBox(gClient->GetRoot(), this, "Failed", 
		   Form("Failed to open file \"%s\"", fname.Data()), 
		   kMBIconExclamation);
      return;
    }

    // Set the format on the file
    switch (fOutFormat.GetSelected()) {
    case 0: // Default float formatting
      out << std::defaultfloat;
      break;
    case 1: // Fixed format
      out << std::fixed;
      break;
    default: // All other cases we go scientific
      out << std::scientific;
      break;
    }

    // Set the precision
    Int_t prec = fOutPrec.GetIntNumber();
    if (prec >= 0) out << std::setprecision(prec);

    // ROOT script 
    if (type == 0) {
      g->SetDrawOption("apl");
      out << "{\n" 
	  << "   // Generated by DataStealer" << std::endl;
      g->SavePrimitive(out, "");
      out << "}\n"
	  << "// EOF" << std::endl;
    }
    // CSV 
    else {
      out << "# X\tY\t"
	  << (a ? "EXL\tEXH\tEYL\tEYH" :
	      s ? "EX\tEY" : "") << std::endl;
      for (Int_t i = 0; i < g->GetN(); i++) {
	Double_t x, y;
	g->GetPoint(i, x, y);
	out << x << "\t" << y;
	if (a) 
	  out << "\t" << a->GetErrorXlow(i) << "\t" << a->GetErrorXhigh(i) 
	      << "\t" << a->GetErrorYlow(i) << "\t" << a->GetErrorYhigh(i);
	else if (s)
	  out << "\t" << s->GetEX()[i] << "\t" << s->GetEY()[i];
	out << std::endl;
      }
    }
    out.close();
    // std::cout <<  "done"  << std::endl;
    delete g;
  }

  //____________________________________________________________________
  inline void 
  Main::HandleSelect(TGLVEntry* e, Int_t btn)
  {
    (void)btn;
    fPoint = e;
    if (e) 
      fStatus.SetText("Left-click, and drag to define error bars");
    else 
      fStatus.SetText("Left-click to add point");
    fCanvas.GetCanvas()->SetSelected(fPoint);
  }
  //____________________________________________________________________
  inline void 
  Main::HandleSelect()
  {
    HandleSelect(0,0);
  }

  //____________________________________________________________________
  inline void 
  Main::HandleChange()
  {
    fCached = false;
    if (CalcCommon() && fCached) HandleCalc();
  }
  
  //____________________________________________________________________
  inline Bool_t 
  Main::CalcCommon()
  {
    fStatus.SetText("Click to define an axis point");
    if (!IsAxisSet()) { fCached = false; return false; }
    if (fCached)      return false;

    fCalculator.Cache(fXLeft, fXRight, fYBottom, fYTop,
		      fXLog.IsDown(), fYLog.IsDown(),
		      fRound.IsDown(), fRoundBias.GetIntNumber());
    fCached = true;
    return true;
  }
  //____________________________________________________________________
  inline Bool_t
  Main::HandleClear() 
  {
    TIter            next(fPoints.GetList());
    const TGLVEntry* e  = 0;
    TGFrameElement*  fe = 0;
    Point*           p  = 0;
    while ((fe = static_cast<TGFrameElement*>(next()))) {
      // (p = static_cast<Point*>(e->GetUserData())w)) {
      e = static_cast<const TGLVEntry*>(fe->fFrame);
      if (!e) continue;
      p = static_cast<Point*>(e->GetUserData());
      if (!p) continue;
      fCanvas.GetCanvas()->RecursiveRemove(p);
      delete p;
    }
    fPoints.RemoveAll();
    fCanvas.GetCanvas()->ls();
    fCanvas.GetCanvas()->Modified();
    fCanvas.GetCanvas()->Update();
    fCanvas.GetCanvas()->cd();
    return kTRUE;
  }
  
  //____________________________________________________________________
  inline Bool_t
  Main::HandleCalc() 
  {
    if (!IsAxisSet()) {
      new TGMsgBox(gClient->GetRoot(), this, "Axis not defined", 
		   "Please define axis first", kMBIconExclamation);
      return kFALSE;
    }
    // CalcCommon();
    TIter            next(fPoints.GetList());
    const TGLVEntry* e      = 0;
    TGFrameElement*  fe     = 0;
    Point*           p      = 0;
    while ((fe = static_cast<TGFrameElement*>(next()))) {
      // (p = static_cast<Point*>(e->GetUserData())w)) {
      e = static_cast<const TGLVEntry*>(fe->fFrame);
      if (!e) continue;
      p = static_cast<Point*>(e->GetUserData());
      if (!p) continue;
      Double_t xv, yv;
      fCalculator.Pixel2Value(p->fPos.fXpixel, p->fPos.fYpixel, xv, yv);
      p->SetValues(xv, yv);

      Double_t eX, eY;
      if (p->HasXError()) {
	for (size_t i = 0; i < 2; i++) {
	  Store& e = (i == 0 ? p->fEX1 : p->fEX2);
	  fCalculator.Pixel2Error(e.fXpixel, e.fYpixel, xv, yv, eX, eY);
	  e.SetValue(eX, eY);
	}
      }
      if (p->HasYError()) {
	for (size_t i = 0; i < 2; i++) {
	  Store& e = (i == 0 ? p->fEY1 : p->fEY2);
	  fCalculator.Pixel2Error(e.fXpixel, e.fYpixel, xv, yv, eX, eY);
	  e.SetValue(eX, eY);
	}
      }    
      fCanvas.GetCanvas()->cd();
      p->Update();
    }
    return kTRUE;
  }

  //____________________________________________________________________
  inline void 
  Main::DrawImage() 
  {
    TCanvas* c = fCanvas.GetCanvas();
    c->cd();
    c->SetLeftMargin(0);
    c->SetRightMargin(0);
    c->SetTopMargin(0);
    c->SetBottomMargin(0);    
    fImage->Draw();
    c->Range(0, 0,fImage->GetWidth(),fImage->GetHeight());
    DoZoom(0, fImage->GetWidth()/2, fImage->GetHeight()/2, 0);
    gClient->NeedRedraw(&fCanvas, kTRUE);
  }
}

#endif /* DATASTEALER_MAIN */
//
// EOF
//

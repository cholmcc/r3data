// -*- mode: C++ -*- 
//
//  A class for grabbing data from scanned graphs
//  Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
// This code was heavily inspired by G3Data available from 
// http://www.frantz.fi/software/g3data.php.  The copyright of G3Data
// follows because some parts of this code is more or less lifted
// directly from there:
//
//  Copyright (C) 2000 Jonas Frantz <jonas.frantz@welho.com>
//
//  This file is part of g3data.
//
//  g3data is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  g3data is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef DATASTEALER_POINT
#define DATASTEALER_POINT
#include <TMarker.h>
#include <TGListView.h>
#include <TLine.h>
#include <TString.h>
#include <TPad.h>
#include "Store.h"

namespace DataStealer 
{
  //====================================================================
  /** @class Point 
      @brief A specialised marker that has a reference to a List view entry.
  */
  struct Point : public TMarker
  {
    /** Constructor
	@param c Container to add to 
	@param n Number */
    Point(TGLVContainer& c, Int_t n);
    /** Destructor */
    virtual ~Point();
    /** Get the name of the marker (number) */
    const char* GetName() const { return fName.Data(); } /* MENU */
    /** Set the pixel values, and update display 
	@param x X pixel coordinate
	@param y Y pixel coordinate */
    void SetPixels(Int_t x, Int_t y);
    /** Set the x and y values */
    void SetValues(Double_t x, Double_t y);
    /** Set pixels and values 
	@param px X pixel coordinate
	@param py Y pixel coordinate 
	@param vx X value 
	@param vy Y value */
    void SetPixelsValues(Int_t px, Int_t py,
			 Double_t vx, Double_t vy);
    /** Set marker color */
    void SetColor(Int_t color);
    /** Set marker style */
    void SetStyle(Style_t style);
    /** Set marker size */
    void SetSize(Float_t size);
    /** Check if we have X errors */
    Bool_t HasXError() const;
    /** Check if we have Y errors */
    Bool_t HasYError() const;
    /** Update list view entry */
    void Update();
    /** Execute an event when cursor does something. */
    void ExecuteEvent(Int_t event, Int_t px, Int_t py);
    /** Clear this point - that is, delete it.
	@param option Passed on to Delete */
    void Clear(Option_t* option="") { Delete(option); }
    /** Print this point to standard out.
	@param option Not used. */
    void Print(Option_t* option="") const;

    /** Position */
    Store fPos;
    /** Lower X error */ 
    Store fEX1;
    /** Upper X error */ 
    Store fEX2;
    /** Lower X error */ 
    Store fEY1;
    /** Upper X error */ 
    Store fEY2;
    /** Are we being moved ?*/ 
    bool fIsMoving;
  protected:
    /** Update the volumns */
    void UpdateCols();
    /** Update the list view */
    void UpdateLV();
    /** Name of entry */
    TString        fName;
    /** Container */
    TGLVContainer& fContainer;
    /** List view entry - argh - must be pointer, deleted by container! */
    TGLVEntry*     fEntry;
    /** Error line */ 
    TLine fEX;
    /** Error line */ 
    TLine fEY;
  
    ClassDef(Point,0);
  };
  
  //__________________________________________________________________
  inline 
  Point::Point(TGLVContainer& c, Int_t n)
    : TMarker(0,0,25), 
      fPos(0,0), 
      fEX1(0,0),
      fEX2(0,0),
      fEY1(0,0),
      fEY2(0,0),
      fName(Form("%d", n)),
      fContainer(c)
  {
    SetNDC();
    SetMarkerColor(2);
    SetMarkerSize(0.6);
    fEntry = new TGLVEntry(&c, fName, fName);
    fEntry->SetViewMode(kLVDetails);
    fEntry->SetUserData(this);
    fContainer.AddItem(fEntry);
    fEX.SetBit(TLine::kLineNDC);
    fEX.SetLineColor(2);
    fEY.SetBit(TLine::kLineNDC);
    fEY.SetLineColor(2);
    fContainer.UnSelectAll();
    // fContainer.Layout();
    // fEntry->Activate(kTRUE);
    // c.SetVsbPosition(Int_t(c.GetHeight())+1);
  }

  //__________________________________________________________________
  inline 
  Point::~Point()
  {
    gPad->RecursiveRemove(&fEX);
    gPad->RecursiveRemove(&fEY);
    fContainer.RemoveItemWithData(this);
    fContainer.UnSelectAll();
    UpdateLV();
  }

  //__________________________________________________________________
  inline void
  Point::SetPixels(Int_t x, Int_t y) 
  {
    fPos.SetPixel(x, y);
    fPos.ClearValue();
    fEX1.ClearPixel();
    fEX2.ClearPixel();
    fEY1.ClearPixel();
    fEY2.ClearPixel();
    gPad->RecursiveRemove(&fEX);
    gPad->RecursiveRemove(&fEY);
    UpdateCols();

    SetX(Double_t(x) / gPad->GetWw());
    SetY(1 - Double_t(y) / gPad->GetWh());
    Draw();
    gPad->Modified();
    gPad->Update();
    gPad->cd();
  }

  //__________________________________________________________________
  inline void
  Point::SetPixelsValues(Int_t px, Int_t py, Double_t vx, Double_t vy) 
  {
    SetPixels(px,py);
    SetValues(vx,vy);
  }

  //__________________________________________________________________
  inline void
  Point::SetValues(Double_t x, Double_t y) 
  {
    fPos.SetValue(x, y);
    UpdateCols();
  }
  //__________________________________________________________________
  inline void
  Point::SetColor(Int_t color) 
  { 
    SetMarkerColor(color);
    fEX.SetLineColor(color);
    fEY.SetLineColor(color);
    gPad->Modified();
    gPad->Update();
  }
  //__________________________________________________________________
  inline void
  Point::SetStyle(Style_t style) 
  { 
    SetMarkerStyle(style);
    gPad->Modified();
    gPad->Update();
  }
  //__________________________________________________________________
  inline void
  Point::SetSize(Float_t size) 
  { 
    SetMarkerSize(size);
    gPad->Modified();
    gPad->Update();
  }
  //__________________________________________________________________
  inline Bool_t
  Point::HasXError() const 
  {
    return (fEX1.fXpixel != fEX2.fXpixel || fEX1.fYpixel != fEX2.fYpixel);
  }
    //__________________________________________________________________
  inline Bool_t
  Point::HasYError() const 
  {
    return (fEY1.fXpixel != fEY2.fXpixel || fEY1.fYpixel != fEY2.fYpixel);
  }
  //__________________________________________________________________
  inline void
  Point::Update()
  {
    if (HasXError()) {
      fEX.SetX1(double(fEX1.fXpixel) / gPad->GetWw());
      fEX.SetX2(double(fEX2.fXpixel) / gPad->GetWw());
      fEX.SetY1(1 - double(fEX1.fYpixel) / gPad->GetWh());
      fEX.SetY2(1 - double(fEX2.fYpixel) / gPad->GetWh());
      fEX.Draw();
    }
    if (HasYError()) {
      fEY.SetX1(double(fEY1.fXpixel) / gPad->GetWw());
      fEY.SetX2(double(fEY2.fXpixel) / gPad->GetWw());
      fEY.SetY1(1 - double(fEY1.fYpixel) / gPad->GetWh());
      fEY.SetY2(1 - double(fEY2.fYpixel) / gPad->GetWh());
      fEY.Draw();
    }
    if (HasXError() || HasYError()) {
      gPad->Modified();
      gPad->Update();
      gPad->cd();
    }
    UpdateCols();
  }
  //__________________________________________________________________
  inline void
  Point::ExecuteEvent(Int_t event, Int_t px, Int_t py) { 
    TMarker::ExecuteEvent(event, px, py);
    switch (event) {
    case kButton1Down:
      fIsMoving = kTRUE;
      // No break !!!
    case kMouseMotion:   break;
    case kButton1Motion: break;
    case kButton1Up:
      SetPixels(px,py);
      break;
    }
    Update();
  }
  //__________________________________________________________________
  inline void
  Point::UpdateCols()
  {
    fEntry->SetSubnames(fPos.fXpstr.Data(), fPos.fYpstr.Data(), 
			fPos.fXvstr.Data(), fPos.fYvstr.Data(), 
			(HasXError() ? fEX1.fXvstr.Data() : ""), 
			(HasXError() ? fEX2.fXvstr.Data() : ""),
			(HasYError() ? fEY1.fYvstr.Data() : ""), 
			(HasYError() ? fEY2.fYvstr.Data() : ""));
    UpdateLV();
  }
  //__________________________________________________________________
  inline void
  Point::UpdateLV()
  {
    TGListView* v = fContainer.GetListView();
    // v->SetViewMode(kLVList);
    v->SetViewMode(kLVDetails);
    v->Layout();
    v->Resize(v->GetDefaultSize());
    gClient->NeedRedraw(v);
  }
  //__________________________________________________________________
  inline void
  Point::Print(Option_t*) const 
  {
#if 0
    std::cout << "  Position: "; fPos.Print();
    std::cout << "  ErrorX 1: "; fEX1.Print();
    std::cout << "  ErrorX 2: "; fEX2.Print();
    std::cout << "  ErrorY 1: "; fEY1.Print();
    std::cout << "  ErrorY 2: "; fEY2.Print();
#endif
  }
}
#endif /* DATASTEALER_POINT */
//
// EOF
//
